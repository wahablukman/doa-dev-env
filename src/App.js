import React, { Component } from 'react';
import { withRouter, Route } from 'react-router-dom';
import '../node_modules/slick-carousel/slick/slick-theme.css';
import '../node_modules/slick-carousel/slick/slick.css';
import './App.css';

import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';
import Homepage from './components/homepage/Homepage';
import ContactUs from './components/contact/Contact';
import About from './components/about/About';
import Locations from './components/location/Location';
import ProductsAndServices from './components/productsAndServices/ProductsAndServices';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        {/* Navbar */}
        <Route exact path="/" component={Homepage} />
        <Route path="/about" component={About} />
        <Route path="/contact" component={ContactUs} />
        <Route path="/location" component={Locations} />
        <Route path="/products-and-services" component={ProductsAndServices} />
        {/* Content */}
        {this.props.location.pathname !== '/' ? <Footer /> : null}
        {/* Footer */}
      </div>
    );
  }
}

export default withRouter(App);
