import React, { Component } from 'react';

class Footer extends Component {
  state = {};
  render() {
    return (
      <footer>
        <div className="content">
          <div className="p-text">
            © {new Date().getFullYear()} Dinamika Oscar Agung
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
