import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../../assets/images/logo.png';
import { CSSTransitionGroup } from 'react-transition-group';

class Navbar extends Component {
  state = {
    menuOpened: false,
    screenWidth: window.innerWidth
  };

  componentDidMount() {
    if (this.state.screenWidth > 500) {
      this.setState({
        menuOpened: true
      });
    }
    // update screen dimension
    window.addEventListener('resize', this.updateMenuOnWidthChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateMenuOnWidthChange);
  }

  updateMenuOnWidthChange = () => {
    this.setState(
      {
        screenWidth: window.innerWidth
      },
      () => {
        if (this.state.screenWidth < 501) {
          this.setState({
            menuOpened: false
          });
        } else {
          this.setState({
            menuOpened: true
          });
        }
      }
    );
  };

  onClickMenu = () => {
    if (this.state.screenWidth < 501) {
      this.setState({
        menuOpened: !this.state.menuOpened
      });
    }
  };

  onClickMenuLogo = () => {
    if (this.state.screenWidth < 501) {
      if (this.state.menuOpened) {
        this.setState({
          menuOpened: false
        });
      }
    }
  };

  render() {
    const navbar = (
      <nav key="#navbar">
        <div className="content">
          <ul className="flex-parent">
            <li onClick={this.onClickMenu}>
              <NavLink exact to="/about">
                About Us
              </NavLink>
            </li>
            <li onClick={this.onClickMenu}>
              <NavLink exact to="/products-and-services">
                Products
              </NavLink>
            </li>
            <li className="logo-on-nav">
              <NavLink exact to="/">
                <img src={Logo} className="logo" alt="" />
              </NavLink>
            </li>
            <li onClick={this.onClickMenu}>
              <NavLink exact to="/contact">
                Contact
              </NavLink>
            </li>
            <li onClick={this.onClickMenu}>
              <NavLink exact to="/location">
                Location
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    );
    return (
      <React.Fragment>
        <div className="mobile-only-nav">
          <NavLink onClick={this.onClickMenuLogo} exact to="/">
            <img src={Logo} className="logo" alt="" />
          </NavLink>
          <i
            onClick={this.onClickMenu}
            className="menu-mobile-button fas fa-caret-down"
          />
        </div>
        <CSSTransitionGroup
          transitionName={{
            enter: 'animated',
            enterActive: 'slideInDown',
            leave: 'animated',
            leaveActive: 'slideOutUp'
          }}
          transitionEnterTimeout={550}
          transitionLeaveTimeout={550}
        >
          {this.state.menuOpened ? navbar : null}
        </CSSTransitionGroup>
      </React.Fragment>
    );
  }
}

export default Navbar;
