import React, { Component } from 'react';
import axios from 'axios';
import keys from '../../config/keys';
import Loader from '../../assets/images/loader.svg';
import { CSSTransitionGroup } from 'react-transition-group';

class Contact extends Component {
  state = {
    _subject: '',
    _honey: '',
    _replyto: '',
    name: '',
    email: '',
    phone: '',
    message: '',
    successMessage: '',
    errorMessage: '',
    loading: false
  };

  componentDidMount() {
    document.title = `Contact`;
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = async e => {
    e.preventDefault();
    await this.setState({
      loading: true
    });
    let formData = new FormData();

    formData.append('_subject', `${this.state.name} sent an enquiry`);
    formData.append('_replyto', this.state.email);
    formData.append('name', this.state.name);
    formData.append('email', this.state.email);
    formData.append('phone', this.state.phone);
    formData.append('message', this.state.message);

    if (this.state._honey !== '') {
      return this.setState(
        {
          errorMessage: 'Form not submitted, possible SPAM',
          loading: false
        },
        () => {
          setTimeout(() => {
            this.setState({
              errorMessage: ''
            });
          }, 5000);
        }
      );
    } else {
      try {
        const submittedForm = await axios.post(
          `https://formsapi.jabwn.com/key/${keys.DOAJABWNAPIKey}`,
          formData
        );
        if (submittedForm.data.message === 'success') {
          this.setState(
            {
              loading: false,
              successMessage: `Enquiry submitted! We'll get back to you as soon as possible`,
              name: '',
              email: '',
              phone: '',
              message: ''
            },
            () => {
              setTimeout(() => {
                this.setState({
                  successMessage: ''
                });
              }, 5000);
            }
          );
        } else {
          this.setState(
            {
              loading: false,
              errorMessage:
                'There is an error in sending your enquiry, try again in a bit'
            },
            () => {
              setTimeout(() => {
                this.setState({
                  errorMessage: ''
                });
              }, 5000);
            }
          );
        }
      } catch (err) {
        return this.setState(
          {
            loading: false,
            errorMessage: err
          },
          () => {
            setTimeout(() => {
              this.setState({
                errorMessage: ''
              });
            }, 5000);
          }
        );
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="contact other-than-homepage text-middle">
          <div className="homepage-background-overlay" />
          <div className="homepage-background" />
          <div className="content flex-parent-column header-wrapper">
            <div className="huge-company-name header animated fadeIn">
              Contact Us
            </div>
          </div>
          <div className="sb-150" />
          <div className="full-size">
            <div className="content">
              <div className="product-type-header">
                <div className="title-text d-inline-block">
                  Fill in the contact form
                </div>
              </div>
              <div className="sb-40" />
              <form noValidate onSubmit={this.onSubmit}>
                <input
                  type="text"
                  name="_honey"
                  onChange={this.onChange}
                  style={{ display: 'none' }}
                />
                <label>Name</label>
                <input
                  onChange={this.onChange}
                  type="text"
                  name="name"
                  value={this.state.name}
                />
                <label>E-mail</label>
                <input
                  onChange={this.onChange}
                  type="email"
                  name="email"
                  value={this.state.email}
                />
                <label>Phone</label>
                <input
                  onChange={this.onChange}
                  type="text"
                  name="phone"
                  value={this.state.phone}
                />
                <label>Enquiry</label>
                <textarea
                  onChange={this.onChange}
                  rows="10"
                  type="text"
                  name="message"
                  value={this.state.message}
                />
                <div className="sb-30" />
                <button type="submit">
                  {this.state.loading ? (
                    <div className="loading-spinner">
                      <img src={Loader} alt="Loading..." />
                    </div>
                  ) : null}
                  Send Enquiry
                </button>
              </form>
              <div className="sb-150" />
            </div>
          </div>
        </div>
        <CSSTransitionGroup
          transitionName={{
            enter: 'animated',
            enterActive: 'fadeIn',
            leave: 'animated',
            leaveActive: 'fadeOut'
          }}
          transitionEnterTimeout={550}
          transitionLeaveTimeout={550}
        >
          {this.state.successMessage ? (
            <div className="success-message flex-parent">
              <div className="success-icon d-inline-block">
                <i className="fas fa-check" />
              </div>
              <div className="success-text d-inline-block">
                {this.state.successMessage}
              </div>
            </div>
          ) : null}
        </CSSTransitionGroup>
        <CSSTransitionGroup
          transitionName={{
            enter: 'animated',
            enterActive: 'fadeIn',
            leave: 'animated',
            leaveActive: 'fadeOut'
          }}
          transitionEnterTimeout={550}
          transitionLeaveTimeout={550}
        >
          {this.state.errorMessage ? (
            <div className="error-message flex-parent">
              <div className="error-icon d-inline-block">
                <i className="fas fa-times" />
              </div>
              <div className="error-text d-inline-block">
                {this.state.errorMessage ? this.state.errorMessage : null}
              </div>
            </div>
          ) : null}
        </CSSTransitionGroup>
      </React.Fragment>
    );
  }
}

export default Contact;
