import React, { Component } from 'react';
import classnames from 'classnames';
import Slider from 'react-slick';
import sliderMitsubishiImg from '../../assets/images/homepage-MITSUBISHI-ELECTRIC.png';
import sliderMinebeaImg from '../../assets/images/homepage-MINEBEA.jpg';
import sliderNSDImg from '../../assets/images/homepage-NSD.png';
import sliderYaskawaImg from '../../assets/images/homepage-YASKAWA.png';
import sliderSmartRayImg from '../../assets/images/homepage-SMARTRAY.png';
import sliderAMETEKImg from '../../assets/images/homepage-AMETEK.png';
import sliderFactoryMaintenance from '../../assets/images/homepage-factory-maintenance.png';
import sliderCustomFabrication from '../../assets/images/homepage-custom-fabrication.png';

class Homepage extends Component {
  state = {
    animated: false,
    screenHeight: window.innerHeight - 110,
    screenWidth: window.innerWidth
  };

  componentDidMount() {
    const { screenWidth } = this.state;
    document.title = 'Dinamika Oscar Agung';
    this.setState({
      animated: true
    });
    if (screenWidth < 501) {
      this.setState({
        screenHeight: window.innerHeight
      });
    }
    window.addEventListener('resize', this.updateMenuOnHeightChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateMenuOnHeightChange);
  }

  updateMenuOnHeightChange = () => {
    const { screenWidth } = this.state;
    if (screenWidth < 501) {
      this.setState({
        screenHeight: window.innerHeight,
        screenWidth: window.innerWidth
      });
    } else {
      this.setState({
        screenHeight: window.innerHeight - 110,
        screenWidth: window.innerWidth
      });
    }
  };

  render() {
    const { animated } = this.state;
    const sliderSettings = {
      dots: false,
      infinite: true,
      speed: 2000,
      autoplay: true,
      autoplaySpeed: 5500,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: false
    };
    return (
      <div
        className="homepage"
        style={{ height: `${this.state.screenHeight}px` }}
      >
        <div className="homepage-background-overlay" />
        <div className="homepage-background" />
        <Slider {...sliderSettings} className="content flex-parent-column">
          <div className="slide-wrapper">
            <div className="huge-company-name">
              <div
                className={classnames('d-inline-block', {
                  'animated fadeInLeft': animated
                })}
              >
                Dinamika
              </div>{' '}
              <div
                className={classnames('d-inline-block', {
                  'animated fadeInLeft delay-03s': animated
                })}
              >
                Oscar
              </div>{' '}
              <br />{' '}
              <div
                className={classnames('d-inline-block', {
                  'animated fadeInLeft delay-06s': animated
                })}
              >
                Agung
              </div>
            </div>
            <div
              className={classnames('brief-description-company', {
                'animated fadeIn delay-06s': animated
              })}
            >
              Providing solution for companies who demands{' '}
              <span className="blue"> well known parts</span> from{' '}
              <span className="red"> Japan</span>
            </div>
          </div>
          {/* 2 */}
          <div className="slide-wrapper">
            <img src={sliderNSDImg} alt="" />
          </div>
          {/* 3 */}
          <div className="slide-wrapper">
            <img src={sliderYaskawaImg} alt="" />
          </div>
          {/* 4 */}
          <div className="slide-wrapper">
            <img src={sliderMitsubishiImg} alt="" />
          </div>
          <div className="slide-wrapper">
            <img src={sliderSmartRayImg} alt="" />
          </div>
          <div className="slide-wrapper">
            <img src={sliderAMETEKImg} alt="" />
          </div>
          {/* 5 */}
          <div className="slide-wrapper">
            <img src={sliderMinebeaImg} alt="" />
          </div>
          <div className="slide-wrapper">
            <img src={sliderFactoryMaintenance} alt="" />
          </div>
          <div className="slide-wrapper">
            <img src={sliderCustomFabrication} alt="" />
          </div>
        </Slider>
        {/* <div className="nav-slider">
              <div className="prev-button d-inline-block">
                <i className="fas fa-angle-left" />
              </div>
              <div className="next-button d-inline-block">
                <i className="fas fa-angle-right" />
              </div>
            </div> */}
      </div>
    );
  }
}

export default Homepage;
