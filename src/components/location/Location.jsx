import React, { Component } from 'react';

class Location extends Component {
  state = {};
  componentDidMount() {
    document.title = `Location`;
  }
  render() {
    return (
      <div className="location other-than-homepage">
        <div className="homepage-background-overlay" />
        <div className="homepage-background" />
        <div className="content flex-parent-column header-wrapper">
          <div className="huge-company-name header animated fadeIn">
            Location
          </div>
        </div>
        <div className="sb-150" />
        <div className="full-size">
          <div className="content">
            <div className="product-type-header">
              <div className="title-text d-inline-block">Our Location</div>
            </div>
            <div className="sb-40" />
            <div className="p-text">
              PT. Dinamika Oscar Agung <br />
              Jl. Agung Niaga 4 Blok G4/37 <br />
              Sunter Agung <br />
              Jakarta 14350 <br />
              <br />
              +6221 658 358 92/93
            </div>
            <div className="sb-70" />
          </div>
        </div>
        <div className="full-size google-map">
          <div className="mapouter">
            <div className="gmap_canvas">
              {/* eslint-disable-next-line */}
              <iframe
                height="500"
                id="gmap_canvas"
                src="https://maps.google.com/maps?q=Jl.%20Agung%20Niaga%204%20Blok%20G4%2F37&t=&z=14&ie=UTF8&iwloc=&output=embed"
                frameBorder="0"
                scrolling="no"
                marginHeight="0"
                marginWidth="0"
              />
            </div>
          </div>
        </div>
        <div className="sb-150" />
      </div>
    );
  }
}

export default Location;
