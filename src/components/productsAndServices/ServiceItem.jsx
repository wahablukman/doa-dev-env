import React, { Component } from 'react';

class ServiceItem extends Component {
  state = {};
  render() {
    const { src, alt, serviceName, info } = this.props;
    return (
      <div className="product services">
        <div className="service-image">
          <div className="black-bg-overlay" />
          <div className="service-text">{serviceName} <span>{info}</span> </div>
          <img src={src} alt={alt} />
        </div>
      </div>
    );
  }
}

export default ServiceItem;
