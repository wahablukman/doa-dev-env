import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Collapsible from 'react-collapsible';
import renderHTML from 'react-render-html';

import ProductItem from './ProductItem';
import ServiceItem from './ServiceItem';
import RawMaterialsItem from './RawMaterialsItem';

import LFXVSeries from '../../assets/images/LFXV-Series.png';
import LFX3Series from '../../assets/images/LFX3-Series.png';
import LDRPFSeries from '../../assets/images/LDR-PF-Series.png';
import LDRPFLASeries from '../../assets/images/LDR-PF-LA-Series.png';
import HPRPFSeries from '../../assets/images/HPR-PF-Series.png';
import LDLPFSeries from '../../assets/images/LDL-PF-Series.png';
import HPDPFSeries from '../../assets/images/HPD-PF-Series.png';
import LFVPFSeries from '../../assets/images/LFV-PF-Series.png';
import Fastus from '../../assets/images/Fastus.png';
import LAK1 from '../../assets/images/LAK-1.png';
import LAK2 from '../../assets/images/LAK-2.png';
import LAK3 from '../../assets/images/LAK-3.png';
import LAK4 from '../../assets/images/LAK-4.png';

import curelascometer7R from '../../assets/images/Curelascometer-7R.jpg';
import curelascometer7P from '../../assets/images/Curelascometer-7P.jpg';

import pulseCountEZABSO from '../../assets/images/Pulse-Count-Selectable-Electro-Magnetic-Rotary-Sensor-ezABSO.jpg';
import singleTurnSensor from '../../assets/images/single-turn-type-absocoder.png';
import multiTurnSensor from '../../assets/images/multi-turn-type-absocoder-sensor.png';
import eddyCurrentEZGAP from '../../assets/images/Eddy-Current-Displacement-Sensor-EZ-GAP.jpg';
import advatangeEZGAPFile from '../../assets/files/EZGap-Advantage.pdf';
import yaskawaServopack5 from '../../assets/images/yaskawa-servopack-sigma-5.png';
import yaskawaServopack7 from '../../assets/images/yaskawa-servopack-sigma-7.png';

import varispeedG7 from '../../assets/images/varispeed-G-7.png';
import A1000 from '../../assets/images/A1000.png';
import U1000 from '../../assets/images/U1000.jpg';
import yaskawaE7 from '../../assets/images/yaskawa-E-7.jpg';
import yaskawaEV from '../../assets/images/yaskawa-E-V.jpg';
import MP3100 from '../../assets/images/MP3100.png';
import MP3300 from '../../assets/images/MP3300.jpg';
import E1000 from '../../assets/images/E-1000.png';

import AMETEKSmartAdvisor from '../../assets/images/AMETEK-smartadvisor.jpg';
import AMETEKSmartView from '../../assets/images/AMETEK-smartview.jpg';
import AMETEKPWIAM from '../../assets/images/AMETEK-Surface-Vision-Plastics-Brochure.png';
import AMETEKNWIAM from '../../assets/images/AMETEK-Surface-Vision-Non-Wovens-Brochure.png';
import AMETEKMSI from '../../assets/images/AMETEK-Surface-Vision-Metals-Brochure.png';
import AMETEKPS from '../../assets/images/AMETEK-Surface-Vision-Metals-Brochure.png';
import AMETEKPWIAMPDF from '../../assets/files/Surface-Vision-Plastic-Brochure.pdf';
import AMETEKNWIAMPDF from '../../assets/files/Surface-Vision-Nonwoven-Brochure.pdf';
import AMETEKMSIPDF from '../../assets/files/Surface-Vision-Metals-Brochure.pdf';
import AMETEKPSPDF from '../../assets/files/Surface-Vision-Paper-Brochure.pdf';

import SMARTRAYEcco95 from '../../assets/images/ecco-95.png';
import SMARTRAYEcco75 from '../../assets/images/ecco-75.png';
import SMARTRAYEcco5535 from '../../assets/images/ecco-55-35.png';
import SMARTRAYEcco95PDF from '../../assets/files/DS_ECCO_95.pdf';
import SMARTRAYEcco75PDF from '../../assets/files/DS_ECCO_75.pdf';
import SMARTRAYEcco55PDF from '../../assets/files/DS_ECCO_55.pdf';
import SMARTRAYEcco35PDF from '../../assets/files/DS_ECCO_35.pdf';

import industrialAirconSystem from '../../assets/images/industrial-aircon-system.png';
import airCurtain from '../../assets/images/air-curtain.png';

import mitsubishiInverter from '../../assets/images/mitsubishi-electric-inverter.png';
import nihonDoki from '../../assets/images/nihon-doki.png';
import tajima from '../../assets/images/tajima.png';
import sanbyAutoStampHolder from '../../assets/images/sanby-auto-stamp-holder.jpg';
import customFabrication from '../../assets/images/custom-fabrication.jpg';
import factoryMaintenance from '../../assets/images/factory-maintenance.jpg';
import pdfIcon from '../../assets/images/pdf-icon.svg';
import FASensorSolutionFile from '../../assets/files/2018FASensorSolution.pdf';
import TireSensorSolutionFile from '../../assets/files/2018TireSensorSolution.pdf';

import evaBag from '../../assets/images/eva-bag.jpg';

class ProductsAndServices extends Component {
  state = {
    modalImage: '',
    modalOpened: false,
    modalInfo: '',
    services: [
      {
        name: 'Factory Maintenance',
        image: factoryMaintenance,
        info:
          'Our company can provide wide range of maintenance for your production machine starting such as software update, parts renewal, and re-calibration for your company.'
      },
      {
        name: 'Custom Fabrication',
        image: customFabrication,
        info:
          'Within our CNC machine capability, we are able to supply your wide range of company needs. In the past, we have produced cust rims, bolts, blades, and many more.'
      }
    ],
    products: [
      {
        title: '',
        image: LFXVSeries,
        brand: 'CCS Inc. – Creating Customer Satisfaction - LFXV Series',
        info: `For more information about the product line-up, please visit <a href="https://www.ccs-grp.com/products/series/301#lineup">CCS LFXV Series website here</a>`
      },
      {
        title: '',
        image: LFX3Series,
        brand: 'CCS Inc. – Creating Customer Satisfaction - LFX3 Series',
        info: `For product line-up, please visit <a href="https://www.ccs-grp.com/products/series/203#lineup">CCS LFX3 Page here</a>`
      },
      {
        title: 'LDR-PF Series',
        image: LDRPFSeries,
        brand: 'CCS Inc. – Creating Customer Satisfaction - PF series',
        info: `High Power Strobe Lights PF Series.
        <br />
        <ul>
          <li>Ring type</li>
          <li>Peak illuminance achieves 6.5 million lx</li>
          <li>Realizes high-speed and accurate inspection</li>
          <li>Solves your xenon flash lamp problems</li>
          <li>Freely adjustable flash timing enables strobing within the exposure period of the camera</li>
        </ul>
        <br />
        For product line-up, please visit this <a href="https://www.ccs-grp.com/products/series/205#lineup">CCS LDR-PF Series page here</a>
        `
      },
      {
        title: 'LDR-PF-LA Series',
        image: LDRPFLASeries,
        brand: 'CCS Inc. – Creating Customer Satisfaction - PF series',
        info: `High Power Strobe Lights PF Series.
        <br />
        <ul>
          <li>Low-Angle Ring type.</li>
          <li>Peak illuminance achieves 10 million lx.</li>
          <li>Realizes high-speed and accurate inspection.</li>
          <li>Solves your xenon flash lamp problems.</li>
          <li>Freely adjustable flash timing enables strobing within the exposure period of the camera.</li>
        </ul>
        <br />
        For product line-up, please visit this  <a href="https://www.ccs-grp.com/products/series/286#lineup">CCS LDR-PF-LA Series page here</a>
        `
      },
      {
        title: 'HPR-PF Series',
        image: HPRPFSeries,
        brand: 'CCS Inc. – Creating Customer Satisfaction - PF series',
        info: `High Power Strobe Lights PF Series.
        <br />
        <ul>
          <li>Ring type for diffused lighting.</li>
          <li>Peak illuminance achieves 2.5 million lx.</li>
          <li>Realizes high-speed and accurate inspection.</li>
          <li>Solves your xenon flash lamp problems.</li>
          <li>Freely adjustable flash timing enables strobing within the exposure period of the camera.</li>
        </ul>
        <br />
        For product line-up, please visit this  <a href="https://www.ccs-grp.com/products/series/284#lineup">CCS HPR-PF Series page here</a>
        `
      },
      {
        title: 'LDL-PF Series',
        image: LDLPFSeries,
        brand: 'CCS Inc. – Creating Customer Satisfaction - PF series',
        info: `High Power Strobe Lights PF Series.
        <br />
        <ul>
          <li>Bar type.</li>
          <li>Peak illuminance achieves 7 million lx.</li>
          <li>Realizes high-speed and accurate inspection.</li>
          <li>Solves your xenon flash lamp problems.</li>
          <li>Freely adjustable flash timing enables strobing within the exposure period of the camera.</li>
        </ul>
        <br />
        For product line-up, please visit this  <a href="https://www.ccs-grp.com/products/series/282#lineup">CCS LDL-PF Series page here</a>
        `
      },
      {
        title: 'HPD-PF Series',
        image: HPDPFSeries,
        brand: 'CCS Inc. – Creating Customer Satisfaction - PF series',
        info: `High Power Strobe Lights PF Series.
        <br />
        <ul>
          <li>Dome type.</li>
          <li>Peak illuminance achieves 4 million lx.</li>
          <li>Realizes high-speed and accurate inspection.</li>
          <li>Solves your xenon flash lamp problems.</li>
          <li>Freely adjustable flash timing enables strobing within the exposure period of the camera.</li>
        </ul>
        <br />
        For product line-up, please visit this  <a href="https://www.ccs-grp.com/products/series/283#lineup">CCS HPD-PF Series page here</a>
        `
      },
      {
        title: 'LFV-PF Series',
        image: LFVPFSeries,
        brand: 'CCS Inc. – Creating Customer Satisfaction - PF series',
        info: `High Power Strobe Lights PF Series.
        <br />
        <ul>
          <li>Coaxial type.</li>
          <li>Peak illuminance achieves 3.7 million lx.</li>
          <li>Realizes high-speed and accurate inspection.</li>
          <li>Solves your xenon flash lamp problems.</li>
          <li>Freely adjustable flash timing enables strobing within the exposure period of the camera.</li>
        </ul>
        <br />
        For product line-up, please visit this  <a href="https://www.ccs-grp.com/products/series/285#lineup">CCS LFV-PF Series page here</a>
        `
      },
      {
        title: '',
        image: Fastus,
        brand: 'CCS Inc. – Creating Customer Satisfaction - FASTUS',
        info: `CCS FASTUS LED lighting is equipped with a proprietary technology designed to maintain brightness automatically over long periods by detecting the temperature and brightness of the lighting. For more information about this product, please visit <a href="https://www.ccs-grp.com/fastus/">CCS Factus web page here</a>`
      },
      {
        title: 'Curelascometer 7R',
        image: curelascometer7R,
        brand: 'Blue bar products JSR Trading Corporation1',
        info: `CURELASTOMETER® TYPE R is curemeter for rubber which complies with the ISO6502 and JIS K6300-2. It can obtain a variety of information accurately.
        (Minimum/Maximum torque, scorch time, curing speed and optimum curing time).`
      },
      {
        title: 'Curelascometer 7P',
        image: curelascometer7P,
        brand: 'Blue bar products JSR Trading Corporation2',
        info: `CURELASTOMETER® TYPE P can monitor the gelation and hardening processes, and it also can obtain the gelation time and the reaction velocity as parameter for R&D and quality control purposes. Thermosetting resins: Epoxy, Phenol, Polyurethane and Unsaturated polyester etc`
      },
      {
        title: 'Pulse Count Selectable Electro-Magnetic Rotary Sensor ezABSO',
        image: pulseCountEZABSO,
        brand: 'NSD Corporation',
        info: `ezABSO® is a brand new Rotary Encoder which is adopted Electromagnetic Induction Type. Position detection is available ezABSO® singly due to the converter system is incorporated inside.`
      },
      {
        title: 'Single-turn type ABSOCODER Sensor',
        image: singleTurnSensor,
        brand: 'NSD Corporation',
        info: `NSD single turn absolute position sensor named VRE ABSOCODER is an electro-mechanical sensor that measures the angular position under magnetic reluctance change.`
      },
      {
        title: 'Multi-turn type ABSOCODER Sensor',
        image: multiTurnSensor,
        brand: 'NSD Corporation',
        info: `NSD multi turn absolute position sensor named MRE ABSOCODER is an electro-mechanical sensor that measures the angular position under magnetic reluctance change.`
      },
      {
        title: 'Eddy Current Displacement Sensor EZ GAP',
        image: eddyCurrentEZGAP,
        brand: 'NSD Corporation',
        info: `EZ GAP is the eddy current displacement sensor to measure the distance to iron, stainless and aluminium.<br /><br /><h1>Advantage</h1><div className="catalogue-library"><img src=${pdfIcon} alt="" /><a href=${advatangeEZGAPFile} target="_blank" rel="noopener noreferrer"> EZGap-Advantage.pdf </a> </div>`
      },
      {
        title: 'Yaskawa Servopacks Sigma 5',
        image: yaskawaServopack5,
        brand: 'Yaskawa Electric',
        info: ''
      },
      {
        title: 'Yaskawa Servopacks Sigma 7',
        image: yaskawaServopack7,
        brand: 'Yaskawa Electric',
        info: ''
      },
      {
        title: 'Varispeed G-7',
        image: varispeedG7,
        brand: 'Yaskawa Electric - AC Inverter Drives',
        info:
          'The first 400V class general-purpose inverter in the world to use the 3-level control method, to approach sine wave output voltage. It provides the solution to problems like motor insulation damage to surge voltage, and electrolytic corrosion of motor bearings due to shaft voltage. Existing general-purpose motors can be used even without surge suppression filters. The noise and leakage current are greatly reduced (halved in in-house comparison).'
      },
      {
        title: 'A-1000',
        image: A1000,
        brand: 'Yaskawa Electric - AC Inverter Drives',
        info:
          'A top quality drive: silent, beautiful, and incredibly powerful. Perfectly designed functions open a new field with A1000. Integrating the latest vector control technology in a general purpose drive with the performance of a higher order demanded by the drives industry.'
      },
      {
        title: 'E-1000',
        image: E1000,
        brand: 'Yaskawa Electric - AC Inverter Drives',
        info:
          'The Green Inverter with super energy savings yet easy to use and has been in compliance with the latest Environment requirements!'
      },
      {
        title: 'U-1000 - Low Harmonics Regenerative Matrix Converter',
        image: U1000,
        brand: 'Yaskawa Electric - AC Inverter Drives',
        info: `Yaskawa's development of the world's first application of matrix converter technology in 2006 made it possible to solve AC drive problems. Further evolution of this technology has resulted in the U1000.
        This sophisticated series of motor drives available only from Yaskawa eliminates the problems of standard AC drives.
        The U1000 tops the performance of general-purpose AC drives to further improve the performance of your facilities.`
      },
      {
        title: '∑-7 Series',
        image: yaskawaE7,
        brand: 'Yaskawa Electric - AC Servo Drive',
        info: `The Σ-7 series delivers the world's highest performance while providing other features such as further improvements in safety and an environmentally friendly design to meet the changing needs of its users. This series offers solutions that are guaranteed to satisfy users in all kinds of scenarios in the life-cycle of systems.`
      },
      {
        title: '∑-V Series',
        image: yaskawaEV,
        brand: 'Yaskawa Electric - AC Servo Drive',
        info: `The Σ-V series boasts the industry's highest speed response (1.6 kHz) and control stability to enable positioning to be conducted at short intervals and a high level of accuracy. When combined with a rotary servomotor or linear servomotor that achieves a maximum motor speed of 6000 min-1, the Σ-V series can derive the utmost from the performance of machines. This easy-to-use servo series offers significant reductions in the time required for startup, servo adjustments, and troubleshooting `
      },
      {
        title: 'MP3100 - Broad Type Machine Controller',
        image: MP3100,
        brand: 'Yaskawa Electric - Machine Controller',
        info: `Yaskawa's MP3100 Broad Type Machine Controller integrates the motion, vision, and robot systems that are indispensable to machine control to achieve ideal control of machine systems. The integrated management of information allows you to monitor the entire system to improve maintainability and traceability.`
      },
      {
        title: 'MP3300 - Optimal Motion Controller',
        image: MP3300,
        brand: 'Yaskawa Electric - Machine Controller',
        info: `Yaskawa's Leading MP3300 Machine Controller is the successor of the famous MP2000 series which performs one of industry's fastest synchronous scanning. Combined with the Σ-7 series of AC servo drives, rest assured our customers can make the best use of systems with motion control offering a whole new range of exciting possibilities.`
      },
      {
        title: 'AMETEK - SmartAdvisor',
        image: AMETEKSmartAdvisor,
        brand: 'AMETEK - SmartAdvisor',
        info: ''
      },
      {
        title: 'AMETEK - SmartView',
        image: AMETEKSmartView,
        brand: 'AMETEK - SmartView',
        info: ''
      },
      {
        title: 'Industrial Air Conditioning System',
        image: industrialAirconSystem,
        brand: 'Mitsubishi Electric',
        info:
          '*Image shown is for illustration purposes only with VRF system being used. For other products, please contact us.'
      },
      {
        title: 'Air Curtain',
        image: airCurtain,
        brand: 'Mitsubishi Electric',
        info: ''
      },
      {
        title: 'Mitsubishi Electric inverter',
        image: mitsubishiInverter,
        brand: 'Others',
        info: ''
      },
      {
        title: 'Nihon doki Measuring Tape',
        image: nihonDoki,
        brand: 'Others',
        info: ''
      },
      {
        title: 'Tajima Measuring Tape',
        image: tajima,
        brand: 'Others',
        info: ''
      },
      {
        title: 'Sanby Auto Stamp Holder',
        image: sanbyAutoStampHolder,
        brand: 'Others',
        info: `
        “Automatically Inking, Rhythmically Stamping”
        <br />
        Self-Inking Stamps | Auto Stamp Series 
        <br />
        <ul>
          <li>- Re-ink easily, no ink pads required</li>
          <li>- High durable metal body for long-term use</li>
          <li>- Automatically inking each time stamping make an indelible impression</li>
          <li>- Rubber stamp kept inside the body to prevent ink overflow</li>
          <li>- Compact size, portable anywhere</li>
          <li>- Free refill ink (red) 5 ml.</li>
        </ul>`
      }
    ],
    rawMaterials: [
      {
        title: 'EVA Bag',
        image: evaBag,
        brand: 'Others',
        info:
          'EVA Bags are used to store the powder and granule industrial chemicals (carbon black ,white carbon black, calcium carbonate, etc. ) in rubber related industry as it has soft and low melting point. The benefit of using this product is no need to unpack as this particular bag can be put directly into rubber mixing equipment which then will be mixed evenly with rubber without sacrificing the rubber output quality. It also has the benefit to improve the accuracy of rubber blending ratio because it protects the raw material used in the production environment.  To suit your needs, we can also provide custom coloring as marking for your production line.'
      }
    ]
  };
  componentDidMount() {
    document.title = `Products and Services`;
  }

  onClickImage = e => {
    this.setState(
      {
        modalOpened: true,
        modalImage: e.target.closest('.product-image').querySelector('img').src,
        modalInfo: e.target
          .closest('.product-image')
          .querySelector('img')
          .getAttribute('data-info')
      },
      () => {}
    );
  };

  onClickModal = () => {
    this.setState({
      modalOpened: false,
      modalImage: '',
      modalInfo: ''
    });
  };

  render() {
    let { products, services, rawMaterials } = this.state;
    services = services.map(service => {
      return (
        <ServiceItem
          key={service.name}
          src={service.image}
          info={service.info}
          alt={service.name}
          serviceName={service.name}
        />
      );
    });
    const filteredCCSProducts1 = products.filter(
      product =>
        product.brand ===
        'CCS Inc. – Creating Customer Satisfaction - LFXV Series'
    );
    const filteredCCSProducts2 = products.filter(
      product =>
        product.brand ===
        'CCS Inc. – Creating Customer Satisfaction - LFX3 Series'
    );
    const filteredCCSProducts3 = products.filter(
      product =>
        product.brand ===
        'CCS Inc. – Creating Customer Satisfaction - PF series'
    );
    const filteredCCSProducts4 = products.filter(
      product =>
        product.brand === 'CCS Inc. – Creating Customer Satisfaction - FASTUS'
    );
    const filteredBlueBarProducts1 = products.filter(
      product => product.brand === 'Blue bar products JSR Trading Corporation1'
    );
    const filteredBlueBarProducts2 = products.filter(
      product => product.brand === 'Blue bar products JSR Trading Corporation2'
    );
    const filteredNSDProducts = products.filter(
      product => product.brand === 'NSD Corporation'
    );
    // const filteredYaskawaProducts = products.filter(
    //   product => product.brand === 'Yaskawa Electric'
    // );
    const filteredYaskawaACIDProducts = products.filter(
      product => product.brand === 'Yaskawa Electric - AC Inverter Drives'
    );
    const filteredYaskawaACSDProducts = products.filter(
      product => product.brand === 'Yaskawa Electric - AC Servo Drive'
    );
    const filteredYaskawaMCProducts = products.filter(
      product => product.brand === 'Yaskawa Electric - Machine Controller'
    );
    const filteredMitsubishiProducts = products.filter(
      product => product.brand === 'Mitsubishi Electric'
    );
    const filteredOtherProducts = products.filter(
      product => product.brand === 'Others'
    );
    const CCSProducts1 = filteredCCSProducts1.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info}
        />
      );
    });
    const CCSProducts2 = filteredCCSProducts2.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info}
        />
      );
    });
    const CCSProducts3 = filteredCCSProducts3.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info}
        />
      );
    });
    const CCSProducts4 = filteredCCSProducts4.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info}
        />
      );
    });
    const BlueBarProducts1 = filteredBlueBarProducts1.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info}
        />
      );
    });
    const BlueBarProducts2 = filteredBlueBarProducts2.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info}
        />
      );
    });
    const NSDProducts = filteredNSDProducts.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info}
        />
      );
    });
    // const yaskawaProducts = filteredYaskawaProducts.map(product => {
    //   return (
    //     <ProductItem
    //       key={product.title}
    //       src={product.image}
    //       onClick={this.onClickImage}
    //       productName={product.title}
    //       alt={product.title}
    //       info={product.info ? product.info : ''}
    //     />
    //   );
    // });
    const yaskawaACIDProducts = filteredYaskawaACIDProducts.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info ? product.info : ''}
        />
      );
    });
    const yaskawaACSDProducts = filteredYaskawaACSDProducts.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info ? product.info : ''}
        />
      );
    });
    const yaskawaMCProducts = filteredYaskawaMCProducts.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info ? product.info : ''}
        />
      );
    });
    const mitsubishiProducts = filteredMitsubishiProducts.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info ? product.info : ''}
        />
      );
    });
    const otherProducts = filteredOtherProducts.map(product => {
      return (
        <ProductItem
          key={product.title}
          src={product.image}
          onClick={this.onClickImage}
          productName={product.title}
          alt={product.title}
          info={product.info ? product.info : ''}
        />
      );
    });
    const rawMaterialItems = rawMaterials.map(rawMaterial => {
      return (
        <RawMaterialsItem
          key={rawMaterial.title}
          src={rawMaterial.image}
          onClick={this.onClickImage}
          rawMaterialsName={rawMaterial.title}
          alt={rawMaterial.title}
          info={rawMaterial.info ? rawMaterial.info : ''}
        ></RawMaterialsItem>
      );
    });
    console.log(this.state);
    return (
      <React.Fragment>
        {this.state.modalOpened ? (
          <div
            className="modal-image animated fadeIn"
            onClick={this.onClickModal}
          >
            <div className="modal-detail-wrapper">
              <img src={this.state.modalImage} alt="" />
              {this.state.modalInfo ? (
                <div className="modal-info">
                  <h1>Detailed Info</h1> {renderHTML(this.state.modalInfo)}
                </div>
              ) : null}
            </div>
          </div>
        ) : null}
        <div className="products other-than-homepage">
          <div className="homepage-background-overlay" />
          <div className="homepage-background" />
          <div className="content flex-parent-column header-wrapper">
            <div className="huge-company-name header animated fadeIn">
              Products and Services
            </div>
          </div>
          <div className="sb-150" />
          <div className="full-size">
            <div className="content">
              <div className="product-type-header">
                <div className="title-text d-inline-block">
                  Engineering Electrical Products
                </div>
              </div>
              <div className="sb-40" />
              <div className="flex-parent-wrap product-row">
                <div className="p-text">
                  Click the picture to see the product in more detail,
                </div>
                <div className="p-text">
                  We proudly announced that we are authorised distributor for
                  these items:
                </div>
                <div className="sb-30" />
                <Collapsible
                  classParentString="flex-parent-wrap sub-title-text"
                  trigger="CCS Inc. – Creating Customer Satisfaction "
                  open={false}
                >
                  {/* {yaskawaProducts} */}
                  CCS are well known for its lightning inspection products which
                  they have over 2,000 products to fulfil your company needs. In
                  addition, custom order based on each customer requirements are
                  also available. Below are some of their highlighted products:
                  <div className="sb-20" />
                  <div className="sub-product-text">
                    LFX series: Flat Dome Light
                  </div>
                  <strong>LFXV Series</strong>
                  <div className="sb-20" />
                  LFXV Series is the industry leading Flat Dome light with a
                  clear field of view. The range is available in 7 sizes with a
                  total of 28 models to choose from (4 LED Colours: Red, White,
                  Blue and Infrared)
                  {CCSProducts1}
                  <div className="sb-5" />
                  <strong>LFX3 Series</strong>
                  <div className="sb-20" />
                  The LFX3-series Light Units are high-power Flat Dome Lights
                  perfect for fast-moving production lines. The brightness of
                  the white lights has been tripled. <br />
                  <br />
                  Example of LFX3 Series application:
                  <div className="sb-20" />
                  {CCSProducts2}
                  <div className="sub-product-text">
                    PF series : High Power Strobe lighting
                  </div>
                  {CCSProducts3}
                  <div className="sb-20" />
                  <div className="sub-product-text">
                    FASTUS: Sensing lighting
                  </div>
                  {CCSProducts4}
                  <div className="sb-5" />
                  <div className="sub-product-text">
                    LAK Machine Vision Lightning
                  </div>
                  <img src={LAK1} alt="" />
                  <img src={LAK2} alt="" />
                  <img src={LAK3} alt="" />
                  <img src={LAK4} alt="" />
                  <div className="sb-10" />
                  <p>
                    For more information about this product, please visit{' '}
                    <a href="http://www.ccs-asia.com.sg/product/product01_top.html">
                      CCS LAK Machine Lightning Page here
                    </a>
                  </p>
                </Collapsible>
                <div className="sb-20" />
                <Collapsible
                  classParentString="flex-parent-wrap sub-title-text"
                  trigger="JSR Trading Corporation"
                  open={false}
                >
                  As a partner of JSR Trading Corporation in Indonesia, we are
                  able to supply one of their leading products called
                  Curelascometer. Curelascometer is a tool developed by JSR
                  Corporation as the world’s first frictionless sealed
                  rotor-less Curelascometer. Since its development in 1966, this
                  product has been widely used to test both curing
                  characteristics of rubber and hardening characteristics of
                  thermosetting resins. <br />
                  <br />
                  For rubber testing, customer can use : Curelascometer 7R and
                  Curelascometer IID
                  <div className="sb-20" />
                  {/* {yaskawaProducts} */}
                  {BlueBarProducts1}
                  <div className="sb-20" />
                  For thermosetting resin testing, customer can use
                  Curelascometer 7P and Curelasctometer IIDP
                  <div className="sb-20" />
                  {BlueBarProducts2}
                  <br />
                  <br />
                  <p>
                    "CURELASTOMETER" is trademark of JSR corporation which was
                    registered in United States of America and other countries.
                    For more information about these products, feel free to{' '}
                    <Link to="/contact">contact us.</Link>
                  </p>
                </Collapsible>
                <div className="sb-20" />
                <Collapsible
                  classParentString="flex-parent-wrap sub-title-text"
                  trigger="NSD Corporation"
                  open={false}
                >
                  <div className="product-videos">
                    <iframe
                      src="https://www.youtube.com/embed/o2N7CVmnc1U?controls=0"
                      frameBorder="0"
                      allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                      title="NSD Water Gate Part 1"
                    />
                    <iframe
                      src="https://www.youtube.com/embed/NmQReu5qCLw?controls=0"
                      frameBorder="0"
                      allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                      title="NSD Water Gate Part 2"
                    />
                  </div>
                  {NSDProducts}
                </Collapsible>
                <div className="sb-20" />
                <Collapsible
                  classParentString="flex-parent-wrap sub-title-text"
                  trigger="Yaskawa Electric"
                  open={false}
                >
                  {/* {yaskawaProducts} */}
                  <div className="sb-20" />
                  <div className="sub-product-text">AC Inverter Drive</div>
                  {yaskawaACIDProducts}
                  <div className="sb-20" />
                  <div className="sub-product-text">AC Servo Drive</div>
                  {yaskawaACSDProducts}
                  <div className="sb-20" />
                  <div className="sub-product-text">Machine Controller</div>
                  {yaskawaMCProducts}
                </Collapsible>
                <div className="sb-20" />
                <Collapsible
                  classParentString="flex-parent-wrap sub-title-text"
                  trigger="AMETEK Surface Vision"
                  open={false}
                >
                  {/* {yaskawaProducts} */}
                  AMETEK Surface Vision is the world leader in automated online
                  surface inspection solutions. Our broad product range is
                  optimized for the monitoring and inspection of webs and
                  surfaces, and for process surveillance applications.
                  <br />
                  <br />
                  The SmartView® and SmartAdvisor® product lines deliver robust,
                  flexible solutions to continuous production processes across a
                  number of industries, with hundreds of customers and more than
                  2,500 installations worldwide.
                  <br />
                  <br />
                  Our systems have become vital to increasing efficiency,
                  streamlining operations, improving product quality and
                  reducing costs and waste in industrial processes.
                  <div className="sb-20" />
                  <div className="sub-product-text">SmartView®</div>
                  <img src={AMETEKSmartView} alt="" />
                  <div className="sb-10"></div>
                  SmartView® is the industry’s leading surface inspection
                  solution, trusted worldwide to detect, identify and visualize
                  surface defects in real time for a range of materials.
                  <br />
                  <br />
                  Combining state-of-the-art software and hardware into an
                  advanced surface inspection platform, SmartView® provides
                  total vision integration for high-speed defect detection,
                  monitoring and reporting. It delivers robust features,
                  flexible operation and proven, high-quality results.
                  <br />
                  <br />
                  For more information about SmartView, please visit
                  (https://www.ameteksurfacevision.com/products/smartview)
                  <div className="sb-20" />
                  <div className="sub-product-text">SmartAdvisor®</div>
                  <img src={AMETEKSmartAdvisor} alt="" />
                  <div className="sb-10"></div>
                  SmartAdvisor® is a simple, reliable monitoring and inspection
                  solution that maximizes machine efficiency and yield rates.
                  Designed for today’s hands-on problem solver, it delivers
                  significant benefits within weeks.
                  <br />
                  <br />
                  A versatile, easy-to-use vision system, SmartAdvisor® provides
                  high-speed, multi-dimensional video monitoring and process
                  analysis to a range of industries. Within minutes of start-up
                  it boosts machine efficiencies, finds defects, and detects
                  process upsets.
                  <br />
                  <br />
                  For more information about SmartAdvisor, please visit
                  (https://www.ameteksurfacevision.com/products/smartadvisor )
                  <div className="sb-20" />
                  <div className="sub-product-text">Industry Brochures</div>
                  View our latest industry, product and application content as
                  interactive e-magazines and learn more about our automated
                  online surface inspection solutions.
                  <div className="sb-10"></div>
                  <strong>PLASTICS WEB INSPECTION AND MONITORING</strong>
                  <div className="sb-10"></div>
                  <a
                    href={AMETEKPWIAMPDF}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={AMETEKPWIAM} width="40%" alt="" />
                  </a>
                  <div className="sb-10"></div>
                  <a
                    href="https://www.ameteksurfacevision.com/pressreleases/emagazines"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    https://www.ameteksurfacevision.com/pressreleases/emagazines
                  </a>
                  <div className="sb-10"></div>
                  <strong>NONWOVENS WEB INSPECTION AND MONITORING</strong>
                  <div className="sb-10"></div>
                  <a
                    href={AMETEKNWIAMPDF}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={AMETEKNWIAM} width="40%" alt="" />
                  </a>
                  <div className="sb-10"></div>
                  <a
                    href="https://www.ameteksurfacevision.com/pressreleases/emagazines"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    https://www.ameteksurfacevision.com/pressreleases/emagazines
                  </a>
                  <div className="sb-10"></div>
                  <strong>METALS SURFACE INSPECTION</strong>
                  <div className="sb-10"></div>
                  <a
                    href={AMETEKMSIPDF}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={AMETEKMSI} width="40%" alt="" />
                  </a>
                  <div className="sb-10"></div>
                  <a
                    href="https://www.ameteksurfacevision.com/pressreleases/emagazines"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    https://www.ameteksurfacevision.com/pressreleases/emagazines
                  </a>
                  <div className="sb-10"></div>
                  <strong>PAPER SOLUTIONS</strong>
                  <div className="sb-10"></div>
                  <a
                    href={AMETEKPSPDF}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={AMETEKPS} width="40%" alt="" />
                  </a>
                  <div className="sb-10"></div>
                  <a
                    href="https://www.ameteksurfacevision.com/pressreleases/emagazines"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    https://www.ameteksurfacevision.com/pressreleases/emagazines
                  </a>
                  <div className="sb-10"></div>
                  For more information about AMETEK Surface Vision Please visit
                  <a
                    href="https://www.ameteksurfacevision.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    https://www.ameteksurfacevision.com/
                  </a>
                </Collapsible>
                <div className="sb-20" />
                <Collapsible
                  classParentString="flex-parent-wrap sub-title-text"
                  trigger="SmartRay"
                  open={false}
                >
                  {/* {yaskawaProducts} */}
                  SmartRay is a company specializing in Cutting Edge 3D Sensors
                  for Inspection, Guidance, and Measurement. Its ECCO products
                  has been proven to be top notch in the market and widely used
                  by reputable companies such as Audi, BMW, Volkswagen,
                  ThyssenKrupp, and many more.
                  <div className="sb-20" />
                  <div className="sub-product-text">ECCO Family</div>
                  The company’s strategy is to combine new technologies with
                  high quality German engineering to create a range of 3D
                  Sensors that can be fitted anywhere, are quick to set up and
                  easy to deploy.
                  <div className="sb-10"></div>
                  SmartRay Sensors combine laser triangulation with innovative
                  image formation technology to create detailed 3D images that
                  can be processed by any 3rd party vision software. The latest
                  ECCO family brings a new design philosophy to the 3D Sensor
                  market that delivers high performance, small size and
                  lightweight construction.
                  <div className="sb-20"></div>
                  <strong>ECCO 95 Series</strong>
                  <div className="sb-10" />
                  <a
                    href={SMARTRAYEcco95PDF}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={SMARTRAYEcco95} alt="" />
                  </a>
                  <div className="sb-10"></div>
                  <strong>ECCO 75 Series</strong>
                  <div className="sb-10" />
                  <a
                    href={SMARTRAYEcco75PDF}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={SMARTRAYEcco75} alt="" />
                  </a>
                  <div className="sb-10"></div>
                  <strong>ECCO 55 and 35 Series</strong>
                  <div className="sb-10" />
                  <a
                    href={SMARTRAYEcco55PDF}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={SMARTRAYEcco5535} alt="" />
                  </a>
                  <div className="sb-10"></div>
                </Collapsible>
                <div className="sb-20"></div>
                <Collapsible
                  classParentString="flex-parent-wrap sub-title-text"
                  trigger="Mitsubishi Electric (Air Conditioning Systems)"
                  open={false}
                >
                  {mitsubishiProducts}
                </Collapsible>
                <div className="sb-20" />
                <Collapsible
                  classParentString="flex-parent-wrap sub-title-text"
                  trigger="Other parts that we are able to supply"
                  open={false}
                >
                  {otherProducts}
                </Collapsible>
                <div className="sb-20" />
              </div>
              <div className="sb-100" />
              <div className="product-header">
                <div className="title-text d-inline-block">
                  Raw Material and Chemical Materials
                </div>
                <div className="sb-50" />
                <div className="flex-parent-wrap product-row">
                  <div className="sb-50" />
                  {rawMaterialItems}
                </div>
                <div className="p-text">
                  For more info about this or any other products, feel free to{' '}
                  {/* eslint-disable-next-line */}
                  <Link to="/contact">contact us.</Link>
                </div>
              </div>
              <div className="sb-100" />
              <div className="product-header">
                <div className="title-text d-inline-block">Services</div>
                <div className="flex-parent-wrap product-row">
                  <div className="sb-50" />
                  {services}
                </div>
              </div>
              <div className="sb-100" />
              <div className="product-header">
                <div className="title-text d-inline-block">
                  Catalogue Library
                </div>
                <div className="flex-parent-wrap product-row catalogue-row">
                  <div className="sb-50" />
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={AMETEKPWIAMPDF}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Surface-Vision-Plastic-Brochure.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={AMETEKPSPDF}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Surface-Vision-Paper-Brochure.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={AMETEKNWIAMPDF}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Surface-Vision-Nonwoven-Brochure.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={AMETEKMSIPDF}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Surface-Vision-Metals-Brochure.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={SMARTRAYEcco95PDF}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      DS_ECCO_95.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={SMARTRAYEcco75PDF}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      DS_ECCO_75.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={SMARTRAYEcco55PDF}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      DS_ECCO_55.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={SMARTRAYEcco35PDF}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      DS_ECCO_35.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={FASensorSolutionFile}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      NSD_2018FASensorSolution.pdf
                    </a>
                  </div>
                  <div className="catalogue-library">
                    <img src={pdfIcon} alt="" />
                    <a
                      href={TireSensorSolutionFile}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      NSD_2018TireSensorSolution.pdf
                    </a>
                  </div>
                </div>
              </div>
              <div className="sb-150" />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ProductsAndServices;
