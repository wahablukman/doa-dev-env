import React, { Component } from 'react';

class ProductItem extends Component {
  state = {};
  render() {
    const { src, onClick, productName, alt, info } = this.props;
    return (
      <div className="product">
        <div className="product-image-wrapper">
          <div className="product-image" onClick={onClick}>
            <img src={src} alt={alt} data-info={info}  />
            <div className="overlay-info-button">Click for more info</div>
          </div>
        </div>
        <div className="product-name-wrapper p-text">
          {productName}
          {info ? <span>{info}</span> : null}
        </div>
      </div>
    );
  }
}

export default ProductItem;
