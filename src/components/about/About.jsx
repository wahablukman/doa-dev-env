import React, { Component } from 'react';
import AboutImage from '../../assets/images/vision.jpg';
import AboutImage2 from '../../assets/images/mission.jpg';

class About extends Component {
  state = {};
  componentDidMount() {
    document.title = `About`;
  }
  render() {
    return (
      <div className="about other-than-homepage">
        <div className="homepage-background-overlay" />
        <div className="homepage-background" />
        <div className="content flex-parent-column header-wrapper">
          <div className="huge-company-name header animated fadeIn">About</div>
        </div>
        <div className="full-size">
          {/* <div className="about-the-business-bg" /> */}
          <div className="content about-the-business flex-parent">
            <div className="text p-text">
              PT. Dinamika Oscar Agung was established in 2006 to provide
              solution for companies who demands a well known part from Japan.
              With the principle of high level service, the company aims to
              satisfy its customer from the first sales process after sales
              process as a one stop solution for its clients. Towards our vision
              and mission, we expanded to fulfil our customer needs by providing
              broader range of engineering products while adding raw material
              and maintenance solution such as Machine renewal or software
              upgrade to ensure our customers are well served by us.
            </div>
            <div className="title title-text">About the Business</div>
          </div>
        </div>
        <div className="full-size">
          <div className="content about-section visi-misi flex-parent">
            <div className="title-and-text">
              <div className="title title-text">Vision</div>
              <div className="text p-text">
                To become both trusted and accountable business partners for our customers which put their needs above everything{' '}
              </div>
            </div>
            <div className="picture">
              <img src={AboutImage} alt="" />
            </div>
          </div>
        </div>
        <div className="full-size">
          <div className="content about-section visi-misi flex-parent">
            <div className="title-and-text">
              <div className="title title-text">Mission</div>
              <div className="text p-text">
                To build and preserve a lifetime relationship with our respected
                customers; to provide efficient and effective solutions
                accordingly to customers’ needs.
              </div>
            </div>
            <div className="picture">
              <img src={AboutImage2} alt="" />
            </div>
          </div>
        </div>
        <div className="sb-150 mobile-only-space" />
      </div>
    );
  }
}

export default About;
